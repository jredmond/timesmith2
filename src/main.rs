#[cfg(windows)] extern crate winapi;
extern crate scrap;
extern crate image;
extern crate gtk;
extern crate gdk;
extern crate gdk_pixbuf;
#[macro_use]
extern crate relm;
#[macro_use]
extern crate relm_derive;
extern crate chrono;
extern crate rusqlite;
extern crate r2d2;
extern crate r2d2_sqlite;
extern crate schedule_recv;


use relm::{Relm, Widget, Update};
use gtk::{
    Inhibit, 
    Window, 
    WindowType, 
    WidgetExt, 
    Image, 
    ContainerExt, 
    ImageExt, 
    Label, 
    LabelExt};
use gtk::Orientation::Vertical;
use gdk::{EventMask};
use gdk_pixbuf::Pixbuf;

use scrap::{Display, Capturer};

use winapi::um::winuser::{ 
    GetForegroundWindow, 
    MonitorFromWindow, 
    MONITOR_DEFAULTTONEAREST, 
    GetMonitorInfoA, 
    MONITORINFO };

use std::mem;
use std::thread;
use std::io::ErrorKind::WouldBlock;
use std::time::Duration;

use chrono::prelude::*;

use rusqlite::DatabaseName;
use std::io::{Read, Write, Seek, SeekFrom};

use r2d2::PooledConnection;
use r2d2_sqlite::SqliteConnectionManager;


// Testing RELM
// ------------
struct Model {
    display_time: DateTime<Utc>,
    display_rowid: i64,
    db_connection: PooledConnection<SqliteConnectionManager>
}

#[derive(Msg)]
enum Msg {
    Hover(f64),
    Quit,
}

struct Win {
    // …
    model: Model,
    window: Window,
    _image: Image,
    _label: Label,
}

impl Update for Win {
    // Specify the model used for this widget.
    type Model = Model;
    // Specify the model parameter used to init the model.
    type ModelParam = (PooledConnection<SqliteConnectionManager>);
    // Specify the type of the messages sent to the update function.
    type Msg = Msg;

    // Return the initial model.
    fn model(_: &Relm<Self>, param: Self::ModelParam) -> Model {
        Model {
            display_time: Utc::now(),
            display_rowid: 0,
            db_connection: param
        }
    }

    // The model may be updated when a message is received.
    // Widgets may also be updated in this function.
    // Futures and streams can be connected to send a message when a value is ready.
    fn update(&mut self, event: Msg) {
        match event {
            // Msg::SomeEvent => {
            //     let future = create_future();
            //     relm.connect_exec_ignore_err(future, SomeEvent);
            // },
            Msg::Hover(mouse_y) => { 
                if let Some(hover_time) = get_time_at_mouse_y(mouse_y, &self._image) {
                    self.model.display_time = hover_time;
                    let time_string = hover_time.with_timezone(&Local).format("%a %b %e %R").to_string();
                    self._label.set_text(&time_string);
                    let db = &self.model.db_connection;
                    let rowid = rowid_closest_to(hover_time, db);
                    if rowid != self.model.display_rowid {
                        self.model.display_rowid = rowid;
                        let blob = retrieve_blob(rowid, 6220800, db);
                        let mut pixbuf = Pixbuf::new_from_vec(blob, 0, false, 8, 1920, 1080, 6220800/1080);
                        pixbuf = pixbuf.scale_simple(1920/2,1080/2, 2).unwrap();
                        self._image.set_from_pixbuf(Some(&pixbuf));
                    }
                }
            },
            Msg::Quit => gtk::main_quit(),
        }
    }

    // The next method is optional.
    // Futures and streams can be connected when the `Widget` is created in the
    // `subscriptions()` method.
    // fn subscriptions(&mut self, relm: &Relm<Self>) {
    //     let stream = Interval::new(Duration::from_secs(1));
    //     relm.connect_exec_ignore_err(stream, Tick);
    // }
}

impl Widget for Win {
    // Specify the type of the root widget.
    type Root = Window;

    // Return the root widget.
    fn root(&self) -> Self::Root {
        self.window.clone()
    }

    // Create the widgets.
    fn view(relm: &Relm<Self>, model: Self::Model) -> Self {
        // GTK+ widgets are used normally within a `Widget`.
        let window = Window::new(WindowType::Toplevel);
        
        let rowid = rowid_closest_to(model.display_time, &model.db_connection);
        let blob = retrieve_blob(rowid, 6220800, &model.db_connection);
        let mut pixbuf = Pixbuf::new_from_vec(blob, 0, false, 8, 1920, 1080, 6220800/1080);
        pixbuf = pixbuf.scale_simple(1920/2,1080/2, 2).unwrap();
        let screenshot_image = Image::new_from_pixbuf(Some(&pixbuf));
        
        let label = Label::new(None);
        
        let vbox = gtk::Box::new(Vertical, 0);
        vbox.add(&label);
        vbox.add(&screenshot_image);
        window.add(&vbox);

        window.add_events((EventMask::POINTER_MOTION_MASK).bits() as i32);

        connect!(
            relm,
            window,
            connect_motion_notify_event(_, e),
            return (Msg::Hover(e.get_position().1), Inhibit(false))
        );

        // Connect the signal `delete_event` to send the `Quit` message.
        connect!(relm, window, connect_delete_event(_, _), return (Some(Msg::Quit), Inhibit(false)));
        // There is also a `connect!()` macro for GTK+ events that do not need a
        // value to be returned in the callback.

        window.show_all();

        Win {
            model,
            window: window,
            _image: screenshot_image,
            _label: label
        }
    }
}

fn get_time_at_mouse_y(mouse_y: f64, screenshot_image: &Image) -> Option<DateTime<Utc>> {
    let widget_size = screenshot_image.get_allocation();
    let y_ratio = (mouse_y - widget_size.y as f64) / widget_size.height as f64;
    if y_ratio >= 0.0 && y_ratio <= 1.0 {
        
        let start = Local::today().and_hms(8, 0, 0);
        let end = Local::today().and_hms(18, 0, 0);

        let range = end.signed_duration_since(start).num_seconds();

        let offset = range as f64 * y_ratio;

        let hover_time = start + chrono::Duration::seconds(offset as i64);

        return Some(hover_time.with_timezone(&Utc));
    }
    None
}

// ------------


fn main() {
    let manager = SqliteConnectionManager::file("screenshots.db");
    let pool = r2d2::Pool::new(manager).unwrap();

    let pool_clone = pool.clone();
    thread::spawn(move || {
        let conn = pool_clone.get().unwrap();
        let one_min = 60000;
        let tick = schedule_recv::periodic_ms(one_min);

        loop {
            capture_foreground_and_save(&conn);
            tick.recv().unwrap();
        }
    });
    
    Win::run((pool.get().unwrap())).unwrap();
}

fn store_blob(blob_to_write: &Vec<u8>, db: &PooledConnection<SqliteConnectionManager>) {

    let blob_size = blob_to_write.len();

    let _ = db.execute("CREATE TABLE screenshots (time INTEGER, image BLOB)", &[]);

    let now = Utc::now();
    db.execute(&format!("INSERT INTO screenshots (time, image) VALUES (?, ZEROBLOB({}))", blob_size), &[&now]).unwrap();
    let new_rowid = db.last_insert_rowid();
    println!("{}", new_rowid);
    let mut blob = db.blob_open(DatabaseName::Main, "screenshots", "image", new_rowid, false).unwrap();
    blob.write(blob_to_write.as_slice()).unwrap();
}

fn rowid_closest_to(target_date: DateTime<Utc>, db: &PooledConnection<SqliteConnectionManager>) -> i64 {
    let query = format!(
        "SELECT ROWID FROM screenshots ORDER BY abs(strftime('%s', '{0}') - strftime('%s', time)) limit 1;", 
        target_date.to_rfc3339());
    let mut stmt = db.prepare(&query).unwrap();
    let rowid = stmt.query_row(&[], |row| {
        row.get(0)
    }).unwrap();
    rowid
}

fn retrieve_blob(rowid: i64, blob_size: usize, db: &PooledConnection<SqliteConnectionManager>) -> Vec<u8> {

    println!("Retrieving blob at id = {}", rowid);
    let mut blob = db.blob_open(DatabaseName::Main, "screenshots", "image", rowid, false).unwrap();

    blob.seek(SeekFrom::Start(0)).unwrap();
    let mut read_buf = Vec::with_capacity(blob_size);
    let bytes_read = blob.read_to_end(&mut read_buf).unwrap();
    assert_eq!(bytes_read, blob_size);
    read_buf
}

fn capture_foreground_and_save(db: &PooledConnection<SqliteConnectionManager>) {
    let (m_top, m_left) = get_foreground_monitor_top_left();

    let capturers = Display::all().expect("Couldn't retrieve displays.")
        .into_iter()
        .map(|display| (display.top(), display.left(), Capturer::new(display).expect("Couldn't begin capture.")));

    let mut foreground_capturer = capturers.into_iter()
        .find(|&(t, l, _)| t == m_top && l == m_left)
        .map(|(_, _, cap)| cap)
        .unwrap();

    let (w, h, screenshot) = capture_screenshot(&mut foreground_capturer);
    println!("Snap.");

    store_blob(&screenshot, db);

}

fn get_foreground_monitor_top_left() -> (i32, i32) {
    let h_window = unsafe { GetForegroundWindow() };
    let h_monitor = unsafe { MonitorFromWindow(h_window, MONITOR_DEFAULTTONEAREST) };

    let mut mi: MONITORINFO = unsafe { mem::zeroed() };
    mi.cbSize = mem::size_of::<MONITORINFO>() as u32;
    unsafe { GetMonitorInfoA(h_monitor, &mut mi) };

    (mi.rcMonitor.top, mi.rcMonitor.left)
}

fn capture_screenshot(capturer: &mut Capturer) -> (usize, usize, Vec<u8>) {
    let (w, h) = (capturer.width(), capturer.height());
    
    let one_second = Duration::from_secs(1);
    let one_frame = one_second / 60;
    loop {
        let buffer = match capturer.frame() {
            Ok(buffer) => buffer,
            Err(error) => {
                if error.kind() == WouldBlock {
                    // Keep spinning.
                    thread::sleep(one_frame);
                    continue;
                } else {
                    panic!("Capture error: {}", error);
                }
            }
        };
        let mut bitflipped = Vec::with_capacity(w * h * 3);
        let stride = buffer.len() / h;

        for y in 0..h {
            for x in 0..w {
                let i = stride * y + 4 * x;
                bitflipped.extend_from_slice(&[
                    buffer[i + 2],
                    buffer[i + 1],
                    buffer[i],
                ]);
            }
        }
        return (w, h, bitflipped);
    }
}